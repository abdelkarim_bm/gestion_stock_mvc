package com.gestion.Gstock.entites;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity 
@Table (name="table_Client")
public class Client implements Serializable {

	@Id 
	@GeneratedValue 
	private long idClient;
	private String Nom;
	private String Prenom;
	private String Adresse;
	private String Tel;
	private String Mail;
	
	@OneToMany (mappedBy ="client")
	private List <CommandeClient> commandeClients;

	public Client() {
		super();
	}

	public long getId() {
		return idClient;
	}

	public void setId(long id) {
		this.idClient = id;
	}

	public long getIdClient() {
		return idClient;
	}

	public void setIdClient(long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getMail() {
		return Mail;
	}

	public void setMail(String mail) {
		Mail = mail;
	}
}
