package com.gestion.Gstock.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="table_MvtStock")
public class MvtStock implements Serializable {

	@Id 
	@GeneratedValue 
	private long idMvtStock;
	public static final int ENTREE=1;
	public static final int SORTIE=2;
	@Temporal (TemporalType.TIMESTAMP)
	private Date dateMVT;
	private BigDecimal quantite;
	private int TypeMVT;
	
	
	@ManyToOne 
	@JoinColumn (name="idArticle")
	private Article article;

	public long getId() {
		return idMvtStock;
	}

	public void setId(long id) {
		this.idMvtStock = id;
	}

	public long getIdMvtStock() {
		return idMvtStock;
	}

	public void setIdMvtStock(long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}

	public Date getDateMVT() {
		return dateMVT;
	}

	public void setDateMVT(Date dateMVT) {
		this.dateMVT = dateMVT;
	}
	
	

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMVT() {
		return TypeMVT;
	}

	public void setTypeMVT(int typeMVT) {
		TypeMVT = typeMVT;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
