package com.gestion.Gstock.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="table_vente")
public class Vente implements Serializable {

	@Id 
	@GeneratedValue 
	private long idVente;
	private String CodeVente;
	@Temporal  (TemporalType.TIMESTAMP)
	private Date dateVe;

	@OneToMany (mappedBy ="vente")
	private List <LigneVente> ligneVentes;

	public long getId() {
		return idVente;
	}

	public void setId(long id) {
		this.idVente = id;
	}

	public long getIdVente() {
		return idVente;
	}

	public void setIdVente(long idVente) {
		this.idVente = idVente;
	}

	public String getCodeVente() {
		return CodeVente;
	}

	public void setCodeVente(String codeVente) {
		CodeVente = codeVente;
	}

	public Date getDateVe() {
		return dateVe;
	}

	public void setDateVe(Date dateVe) {
		this.dateVe = dateVe;
	}

	public List<LigneVente> getLigneVentes() {
		return ligneVentes;
	}

	public void setLigneVentes(List<LigneVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	}
	
	
}
