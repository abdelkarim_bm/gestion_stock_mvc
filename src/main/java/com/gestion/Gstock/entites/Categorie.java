package com.gestion.Gstock.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity 
@Table (name="table_Categorie")
public class Categorie implements Serializable {

	@Id 
	@GeneratedValue 
	private Long idCategorie;
	private String code;
	private String designation;
	
	@OneToMany (mappedBy ="categorie")
	private List <Article> Articles;
	
	public Categorie() {
	}
	
	public Long getId() {
		return idCategorie;
	}

	public void setId(Long id) {
		this.idCategorie = id;
	}

	public long getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(long idCategorie) {
		this.idCategorie = idCategorie;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {
		return Articles;
	}

	public void setArticles(List<Article> articles) {
		Articles = articles;
	}

	
	
	
}
