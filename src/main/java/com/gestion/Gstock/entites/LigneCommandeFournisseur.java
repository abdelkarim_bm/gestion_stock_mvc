package com.gestion.Gstock.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="table_LigneCommandeFournisseur")
public class LigneCommandeFournisseur implements Serializable {

	@Id 
	@GeneratedValue 
	private long idligneCommandeFournisseur;
	@ManyToOne 
	@JoinColumn (name="idCategorie")
	private Article article ;
	@ManyToOne 
	@JoinColumn (name="idCommandeFournisseur")
	private CommandeFournisseur  commandeFournisseur;
	public long getId() {
		return idligneCommandeFournisseur;
	}

	public void setId(long id) {
		this.idligneCommandeFournisseur = id;
	}

	public long getIdligneCommandeFournisseur() {
		return idligneCommandeFournisseur;
	}

	public void setIdligneCommandeFournisseur(long idligneCommandeFournisseur) {
		this.idligneCommandeFournisseur = idligneCommandeFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
}
