package com.gestion.Gstock.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="table_article")
public class Article implements Serializable {	
	
	@Id 
	@GeneratedValue  
	private Long idArticle;
	
	private String codeArticle;	
	
	private String designation;	
	
	private BigDecimal prixArticleHT;
	
	private BigDecimal tauxTVA;
	
	private BigDecimal prixArticleTTC;
	
	private String photo;
	
	@ManyToOne 
	@JoinColumn (name="idCategorie")
	private Categorie categorie;
	
	public Article() {
	}
	
	
	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdarticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixArticleHT() {
		return prixArticleHT;
	}

	public void setPrixArticleHT(BigDecimal prixArticleHT) {
		this.prixArticleHT = prixArticleHT;
	}

	public BigDecimal getTauxTVA() {
		return tauxTVA;
	}

	public void setTauxTVA(BigDecimal tauxTVA) {
		this.tauxTVA = tauxTVA;
	}

	public BigDecimal getPrixArticleTTC() {
		return prixArticleTTC;
	}

	public void setPrixArticleTTC(BigDecimal prixArticleTTC) {
		this.prixArticleTTC = prixArticleTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public void setIdArticle(long idArticle) {
		this.idArticle = idArticle;
	}
	
	
}
